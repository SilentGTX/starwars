async function catchPlanets() {
  let planetsCount = 0;
  let planets = [];
  for (let i = 1; i <= 6; i++) {
    const response = await fetch(
      `https://swapi.booost.bg/api/planets/?page=${i}`
    );
    const data = await response.json();

    planetsCount += data.results.length;
    planets.push(data.results);
  }

  return {
    count: planetsCount,
    planets: planets.flat(),
  };
}

export default catchPlanets;
